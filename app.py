import psycopg2
from flask import Flask


app = Flask(__name__)


def get_db_connection():
    conn = psycopg2.connect(
        host='db',
        database='flaskapp',
        user='flaskuser',
        password='flaskpassword'
    )
    return conn


@app.route('/')
def hello_world():
    conn = get_db_connection()
    cur = conn.cursor()
    cur.execute('SELECT NOW()')
    current_time = cur.fetchone()
    cur.close()
    conn.close()
    return f'Hello World!  Current time: {current_time}'


if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')

